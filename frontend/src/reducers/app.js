import * as types from '../constants/ActionTypes';

const initialState = {};

export default function app(state = initialState, action) {
	switch (action.type) {
		case types.ACTION:

			return {...state};

		default:
			return state;
	}
}
