import * as types from '../constants/ActionTypes';

export function action() {
	return {
		type: types.ACTION,
	};
}
