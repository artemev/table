import configureStore from './configureStore';

describe("Store suite", function() {
	it("configureStore must return store", function() {
		const store = configureStore();
		const methods = Object.keys(store);

		expect(methods).toContain('subscribe');
		expect(methods).toContain('dispatch');
		expect(methods).toContain('getState');
		expect(methods).toContain('replaceReducer');
	});
});