import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import styles from './SmartTableApp.css';
import Table from '../components/Table';

import * as Actions from '../actions/Actions';

@connect(state => ({
	state: state.app
}))
export default class SmartTableApp extends Component {

	static propTypes = {
		state: PropTypes.object.isRequired,
		dispatch: PropTypes.func.isRequired
	};

	constructor() {
		super();
		this.data = getData();

		// this.onClick = ::this.onClick;
		// this.onInput = ::this.onInput;
		this.state = {
			position: {
				x: 0,
				y: 0
			}
		};
		this.onKeyDown = ::this.onKeyDown;
		window.addEventListener('keydown', this.onKeyDown);
	}

	onKeyDown(event) {

		console.log(event)
		let {position:{x, y}} = this.state;

		if (event.key.indexOf('Arrow') != -1) {

			event.preventDefault();
			this.focus = false;

			if (event.key === 'ArrowRight' && x < 17) {
				x++;
			}

			if (event.key === 'ArrowLeft' && x > 0) {
				x--;
			}

			if (event.key === 'ArrowUp' && y > 0) {
				y--;
			}
			if (event.key === 'ArrowDown' && y < 24) {
				y++;
			}

			this.setState({position: {x, y}});
		} else {

			if (!this.focus) {
				//this.selected.focus();
				//this.focus = true;
				//this.selected.value = this.state.rows[y][x].data;

			}

		}

	}

	render() {
		//const {state, dispatch} = this.props;
		//const actions = bindActionCreators(Actions, dispatch);
		return (
			<div className={styles.smartTableApp}>
				<Table data={this.data} position={this.state.position}/>
			</div>
		)
	}
}



function getData() {
	let data = [];

	for (let i = 0; i < 15; i++) {
		let row = [];
		for (let j = 0; j < 17; j++) {
			row.push(
				{
					data: {
						text: 'dfdf'
					},
					style: {
						color: 'pink'
					}
				}
			)
		}

		data.push(row);
	}
	return data;
}