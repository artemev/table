import React, { Component } from 'react';
import { Provider } from 'react-redux';

import SmartTableApp from './SmartTableApp';

export default class App extends Component {
	render() {
		const {store} = this.props;
		return (
			<Provider store={store}>
				<SmartTableApp/>
			</Provider>
		);
	}
}
