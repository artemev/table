import React, { Component, PropTypes } from 'react';

export default class RatingDropdown extends Component {

	constructor() {
		super();
		this.state = {
			open: false,
			selected:0,
		};
		this.onClick = this.onClick.bind(this);
		this.close = this.close.bind(this);
		this.cancel = this.cancel.bind(this);
	}

	onClick() {
		if(this.state.open){
			this.close()
		} else {
			this.open()
		}
	}

	open(){
		document.addEventListener('click', this.cancel);
		this.setState({open:true});
	}

	close(){
		this.setState({open:false});
	}

	cancel(event){
		if(event.target.className==='dropdown_item') return;

		document.removeEventListener('click', this.cancel);
		this.close()
	}
	onSelect(index){
		this.setState({selected:index});
		this.close();
	}

	render() {

		const {open, selected} = this.state;
		const {items} = this.props;
		if (!open) {
			return (
				<div className='dropdown'>
					<div className='dropdown_btn' onClick={this.onClick}><i className='material-icons icon-grey'>do_not_disturb</i> </div>
				</div>
			)
		}

		return (
			<div className='dropdown -opened'>
				<ul className='dropdown_list'>
					<li className='dropdown_item'><i className='material-icons icon-red'>sentiment_neutral</i>
					</li>
					<li className='dropdown_item'><i className='material-icons icon-blue'>sentiment_dissatisfied</i>
					</li>
					<li className='dropdown_item'><i className='material-icons icon-green'>sentiment_satisfied</i>
					</li>
					<li className='dropdown_item'><i
						className='material-icons icon-grey'>exit_to_app</i></li>
					<li className='dropdown_item'><i
						className='material-icons icon-grey'>do_not_disturb</i></li>
					<li className='dropdown_item'><i
						className='material-icons icon-grey'>visibility_off</i></li>
				</ul>
			</div>
		)
	}
}
