import React, { Component, PropTypes } from 'react';

export default class DefaultDropdown extends Component {

	constructor() {
		super();
		this.state = {
			open: false,
			selected:0,
		};
		this.onClick = this.onClick.bind(this);
		this.close = this.close.bind(this);
		this.cancel = this.cancel.bind(this);
	}

	onClick() {
		if(this.state.open){
			this.close()
		} else {
			this.open()
		}
	}

	open(){
		document.addEventListener('click', this.cancel);
		this.setState({open:true});
	}

	close(){
		this.setState({open:false});
	}

	cancel(event){
		if(event.target.className==='dropdown_item') return;

		document.removeEventListener('click', this.cancel);
		this.close()
	}
	onSelect(index){
		this.setState({selected:index});
		this.close();
	}

	render() {

		const {open, selected} = this.state;
		const {items} = this.props;
		if (!open) {
			return (
				<div className='dropdown'>
					<div className='dropdown_btn' onClick={this.onClick}>{items[selected]}</div>
				</div>
			)
		}

		return (
			<div className='dropdown -opened'>
				<ul className='dropdown_list'>
					{items.map((item, index)=>{
						return <li key={index} className='dropdown_item' onClick={()=>this.onSelect(index)}>{item}</li>
					})}
				</ul>
			</div>
		)
	}
}
