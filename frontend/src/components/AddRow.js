import React, { Component, PropTypes } from 'react';
import styles from './AddRow.css';

export default class AddRow extends Component {

	render() {
		const {text, colSpan, cells} = this.props;
		return (
			<tr className={styles.AddRow}>
				<td className='left-icons'></td>
				<td className='space-cell'></td>
				<td className='title' colSpan={colSpan}>{text}</td>
				{Array.from(new Array(cells)).map((item, index)=>{
					return <td key={index}></td>
				})}
			</tr>

		)
	}
}

AddRow.propTypes = {
	text:PropTypes.string,
	colSpan:PropTypes.number,
	cells:PropTypes.number,
};