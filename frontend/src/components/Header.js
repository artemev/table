import React, { Component, PropTypes } from 'react';
import HeadCell from './HeadCell';
import styles from './Header.css';
import Stats from './Stats';


//todo to constants
const headerFields = [
	{data: {text: 'Касса'}, style:{color:'grey', width:58}},
	{data: {text: 'Телефон гостя'}, style:{color:'blue', width:115}},
	{data: {text: 'Имя гостя'}, style:{color:'blue', width:78}},
	{data: {text: 'Виз.'}, style:{color:'blue', width:30}},
	{data: {text: 'Нов./Пост.'}, style:{color:'blue', width:56}},
	{data: {text: 'Оценка сеанса'}, style:{color:'blue', width:53}},
	{data: {text: 'Название услуги'}, style:{color:'yellow', width:135}},
	{data: {text: 'Бар'}, style:{color:'yellow', width:53}},
	{data: {text: 'Время начала'}, style:{color:'yellow', width:53}},
	{data: {text: 'Комната'}, style:{color:'yellow', width:53}},
	{data: {text: 'Длит.'}, style:{color:'yellow', width:53}},
	{data: {text: 'Цена'}, style:{color:'green', width:59}},
];

const staff = [
	{data: {text: 'Кристина админ'}, style:{color:'pink dark', width:52}},
	{data: {text: 'Влада'}, style:{color:'pink', width:52}},
	{data: {text: 'Юля'}, style:{color:'pink', width:52}},
	{data: {text: 'Александра'}, style:{color:'pink', width:52}},
	{data: {text: 'Кристина'}, style:{color:'pink', width:52}},
];

export default class Header extends Component {
	render() {
		return (
			<thead className={styles.Header}>
			<Stats money={23000} guests={30} newbie={10} time={18} rewards={[1000,500,500,500,200]}/>
			<tr>
				<th className='left-icons'/>
				<th className='left-controls'/>
				{headerFields.map((cell, index)=> {
					return <HeadCell key={index} style={cell.style} data={cell.data}/>
				})}
				{staff.map((cell, index)=> {
					return <HeadCell key={index} style={cell.style} data={cell.data}/>
				})}
				<th className='right-controls'/>
			</tr>
			</thead>
		)
	}
}

