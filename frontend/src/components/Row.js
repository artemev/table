import React, { Component, PropTypes } from 'react';
import BodyCell from './BodyCell';

export default class Row extends Component {

	render() {
		const {cells ,selected} =this.props;
		return (
			<tr>
				<td className='left-icons'>
					<button className="btn-icon"><i className="material-icons -error">error</i></button>
				</td>
				<td className='left-controls'>
					<button className="btn-icon -add"><i className="material-icons">add</i></button>
				</td>
				{cells.map((cell, index)=> {
					return <BodyCell key={index} style={cell.style} data={cell.data} selected={selected===index}/>
				})}
				<td className='right-controls'>
					<button className="btn-icon -clear"><i className="material-icons">clear</i></button>
				</td>
			</tr>
		)
	}
}
