import React, { Component, PropTypes } from 'react';
import classnames from 'classnames';
import DefaultCell from './cells/DefaultCell';
export default class BodyCell extends Component {

	render() {
		const {type, children, data, style, selected} = this.props;
		switch (type) {
			default:
				return (
					<td className={classnames(style.color, selected ? 'selected' : '')}>
						<DefaultCell data={data}>{children}</DefaultCell>
					</td>
				)
		}
	}
}
