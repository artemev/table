import React, { Component, PropTypes } from 'react';
import InputElement from 'react-input-mask';
export default class PhoneInputCell extends Component {

	render() {
		return (
			<div>
				<InputElement mask='+7 (999) 999-99-99'/>
			</div>
		)
	}


}
