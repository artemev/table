import React, { Component, PropTypes } from 'react';

export default class DefaultCell extends Component {

	render() {
		const {data:{text}} = this.props;
		return (
			<div>{text}</div>
		)
	}
}
