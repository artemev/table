import React, { Component, PropTypes } from 'react';
import classnames from 'classnames';
import styles from './Table.css';
import * as CeilTypes from '../constants/CellTypes';
import Header from './Header';
import Row from './Row';
import AddRow from './AddRow';
import DefaultDropdown from './dropdowns/DefaultDropdown';
import RatingDropdown from './dropdowns/RatingDropdown';
import PhoneInputCell from './cells/PhoneInputCell';

export default class Table extends Component {

	static propTypes = {
		state: PropTypes.object,
		dispatch: PropTypes.func
	};

	constructor() {
		super();
		//
		// this.onClick = ::this.onClick;
		// this.onInput = ::this.onInput;
		// this.onKeyDown = ::this.onKeyDown;
		//
		// const header = [
		// 	{name: 'касса', type: CeilTypes.GREY},
		// 	{name: 'телефон гостя', type: CeilTypes.BLUE},
		// 	{name: 'имя гостя', type: CeilTypes.BLUE},
		// 	{name: 'виз.', type: CeilTypes.BLUE},
		// 	{name: 'нов./пост.', type: CeilTypes.BLUE},
		// 	{name: 'оценка сеанса', type: CeilTypes.BLUE},
		// 	{name: 'название услуги', type: CeilTypes.YELLOW},
		// 	{name: 'бар', type: CeilTypes.YELLOW},
		// 	{name: 'время начала', type: CeilTypes.YELLOW},
		// 	{name: 'комната', type: CeilTypes.YELLOW},
		// 	{name: 'длит.', type: CeilTypes.YELLOW},
		// 	{name: 'цена', type: CeilTypes.GREEN},
		// 	{name: 'Кристина', type: CeilTypes.PINK},
		// 	{name: 'Кристина', type: CeilTypes.PINK},
		// 	{name: 'Кристина', type: CeilTypes.PINK},
		// 	{name: 'Кристина', type: CeilTypes.PINK},
		// 	{name: 'Кристина', type: CeilTypes.PINK},
		// 	{name: 'Кристина', type: CeilTypes.PINK},
		// ];
		//
		// const arr = Array.from(Array(header.length).keys())
		// 	.map((obj, index)=> {
		// 		return {data: '', type: header[index].type}
		// 	});
		//
		// const rows = Array.from(Array(25).keys())
		// 	.map(()=> {
		// 		return arr.slice(0).map((item=>Object.assign({}, item)));
		// 	});
		//
		// this.state = {
		// 	header,
		// 	rows,
		// 	position: {x: 0, y: 0}
		// };
		//
		// window.addEventListener('keydown', this.onKeyDown);

		this.state = {
			services: [
				'Ветка сакуры',
				'Парная программа',
				'Стандарт 1900',
				'шампанское',
				'виски',
				'сауна с джакузи',
			],
			rooms: [
				'Без окон',
				'Зеркальная',
				'Красная',
				'БДСМ',
			],
			user: [
				'нов.',
				'пост.',
			]

		}
	}

	onKeyDown(event) {

		let {position:{x, y}} = this.state;

		if (event.key.indexOf('Arrow') != -1) {

			event.preventDefault();
			this.focus = false;

			if (event.key === 'ArrowRight' && x < 17) {
				x++;
			}

			if (event.key === 'ArrowLeft' && x > 0) {
				x--;
			}

			if (event.key === 'ArrowUp' && y > 0) {
				y--;
			}
			if (event.key === 'ArrowDown' && y < 24) {
				y++;
			}

			this.setState({...this.state, position: {x, y}});
		} else {

			if (!this.focus) {
				this.selected.focus();
				this.focus = true;
				this.selected.value = this.state.rows[y][x].data;

			}

		}

	}

	onInput(event) {
		const {position:{x, y}, rows} = this.state;
		const data = this.selected.value;
		let modified = this.state.rows.slice(0);
		const cell = modified[y][x];
		modified[y][x] = {...cell, data};

		this.setState({...this.state, rows: modified});

	}

	setArrayValue(array, index, value) {
		return [].concat(array.slice(0, index), [value], array.slice(index + 1));
	}

	componentDidUpdate() {

	}

	onClick(rowIndex, cellIndex) {
		this.setState({...this.state, position: {y: rowIndex, x: cellIndex}});

	}

	getCellClassName(type) {
		switch (type) {
			case CeilTypes.GREY:
				return 'grey';

			case CeilTypes.BLUE:
				return 'blue';

			case CeilTypes.YELLOW:
				return 'yellow';

			case CeilTypes.GREEN:
				return 'green';

			case CeilTypes.PINK:
				return 'pink';

			default:
				return 'no-class'
		}
	}

	render() {
		const {data, position} = this.props;
		return (
			<div className={styles.Table}>
				<table className='table table-condensed' cellPadding={0} cellSpacing={0}>
					<Header cells={data[0]}/>
					<tbody>
					<tr>
						<td className='left-icons'></td>
						<td className='table_title' colSpan='18'>Гости</td>
					</tr>
					<tr>
						<td className='left-icons'>
							<button className='btn-icon'><i className='material-icons -error'>error</i></button>
						</td>
						<td className='left-controls'>
							<button className='btn-icon -add'><i className='material-icons'>add</i></button>
						</td>

						<td className='grey'>
							<div>12783</div>
						</td>
						<td className='blue'>
							<PhoneInputCell/>
						</td>
						<td className='blue'>
							<div> <input value='Donald Regan'/> </div>
						</td>
						<td className='blue'>
							<div className='visits'>1</div>
						</td>
						<td className='blue'>
							<DefaultDropdown items={this.state.user}/>
						</td>
						<td className='blue'>
							<RatingDropdown/>
						</td>
						<td className='yellow'>
							<DefaultDropdown items={this.state.services}/>
						</td>
						<td className='yellow'>
							<div className='checkbox'></div>
						</td>
						<td className='yellow'>
							<div>
								10:30
							</div>
						</td>
						<td className='yellow'>
							<DefaultDropdown items={this.state.rooms}/>
						</td>
						<td className='yellow'>
							<div>90</div>
						</td>
						<td className='green'>
							<div>1900</div>
						</td>

						<td className='pink dark'>
							<div>1000</div>
						</td>
						<td className='pink'>
							<div>400</div>
						</td>
						<td className='pink'>
							<div></div>
						</td>
						<td className='pink'>
							<div>400</div>
						</td>
						<td className='pink'>
							<div></div>
						</td>

						<td className='right-controls'>
							<button className='btn-icon -clear'><i className='material-icons'>clear</i></button>
						</td>
					</tr>




					<tr>
						<td className='left-icons'></td>
						<td className='left-controls'>
							<button className='btn-icon -add'><i className='material-icons'>add</i></button>
						</td>

						<td className='grey'>
							<div>2500</div>
						</td>
						<td className='blue' colSpan='5'>
							<div className='addition'>дополнение</div>
						</td>
						<td className='yellow'>
							<DefaultDropdown items={this.state.services}/>
						</td>
						<td className='yellow'>
							<div className='checkbox'>
								<i className='material-icons'>done</i>
							</div>
						</td>
						<td className='yellow'>
							<div><i className='material-icons'>remove</i></div>
						</td>
						<td className='yellow'>
							<div><i className='material-icons'>remove</i></div>
						</td>
						<td className='yellow'>
							<div><i className='material-icons'>remove</i></div>
						</td>
						<td className='green'>
							<div>1200</div>
						</td>

						<td className='pink dark'>
							<div>500</div>
						</td>
						<td className='pink'>
							<div></div>
						</td>
						<td className='pink'>
							<div></div>
						</td>
						<td className='pink'>
							<div></div>
						</td>
						<td className='pink'>
							<div></div>
						</td>

						<td className='right-controls'>
							<button className='btn-icon -clear'><i className='material-icons'>clear</i></button>
						</td>
					</tr>



					<tr>
						<td className='left-icons'>
							<button className='btn-icon'><i className='material-icons -error'>error</i></button>
						</td>
						<td className='left-controls'>
							<button className='btn-icon -add'><i className='material-icons'>add</i></button>
						</td>

						<td className='grey'>
							<div><i className='material-icons'>remove</i></div>
						</td>
						<td className='blue'>
							<PhoneInputCell/>
						</td>
						<td className='blue'>
							<div> <input value='Richard Nixon'/> </div>
						</td>
						<td className='blue'>
							<div className='visits'>1</div>
						</td>
						<td className='blue'>
							<DefaultDropdown items={this.state.user}/>
						</td>
						<td className='blue'>
							<RatingDropdown/>
						</td>
						<td className='yellow' colSpan='5'>
							<div className='addition'>Не пустили</div>
						</td>
						<td className='green'>
							<div></div>
						</td>

						<td className='pink dark'>
							<div></div>
						</td>
						<td className='pink'>
							<div></div>
						</td>
						<td className='pink'>
							<div></div>
						</td>
						<td className='pink'>
							<div></div>
						</td>
						<td className='pink'>
							<div></div>
						</td>

						<td className='right-controls'>
							<button className='btn-icon -clear'><i className='material-icons'>clear</i></button>
						</td>
					</tr>


					{/*{data.map((cells, index)=> {*/}
					{/*return <Row key={index} cells={cells} selected={position.y === index ? position.x : -1}/>*/}
					{/*})}*/}

					<AddRow text='Добавить гостя' colSpan={7} cells={10}/>

					<tr className={styles.Penalties}>
						<td className='left-icons'></td>
						<td className='table_title' colSpan='18'>Штрафы и премии</td>
					</tr>
					{/*<Row cells={data[0]}/>*/}
					{/*<Row cells={data[0]}/>*/}

					<tr className='-penaly'>
						<td className='left-icons'></td>
						<td className='left-controls'></td>

						<td className='grey'>
							<div>1800</div>
						</td>
						<td className='' colSpan='10'>
							<div>Штраф за курение в туалете</div>
						</td>
						<td className='green'>
							<div></div>
						</td>

						<td className='pink dark'>
							<div>-200</div>
						</td>
						<td className='pink'>
							<div>-800</div>
						</td>
						<td className='pink'>
							<div></div>
						</td>
						<td className='pink'>
							<div>-800</div>
						</td>
						<td className='pink'>
							<div></div>
						</td>

						<td className='right-controls'>
							<button className='btn-icon -clear'><i className='material-icons'>clear</i></button>
						</td>
					</tr>
					<tr className='-bonus'>
						<td className='left-icons'></td>
						<td className='left-controls'></td>

						<td className='grey'>
							<div>-3000</div>
						</td>
						<td className='' colSpan='10'>
							<div>Премия в честь профессионального праздника</div>
						</td>
						<td className='green'>
							<div></div>
						</td>

						<td className='pink dark'>
							<div>1000</div>
						</td>
						<td className='pink'>
							<div>500</div>
						</td>
						<td className='pink'>
							<div>500</div>
						</td>
						<td className='pink'>
							<div>500</div>
						</td>
						<td className='pink'>
							<div>500</div>
						</td>

						<td className='right-controls'>
							<button className='btn-icon -clear'><i className='material-icons'>clear</i></button>
						</td>
					</tr>
					<AddRow text='Добавить штраф / премию' colSpan={12} cells={5}/>


					{/*{this.state.rows.map((row, rowIndex)=> {*/}
					{/*return <tr key={rowIndex}>*/}
					{/*{row.map((cell, cellIndex)=> {*/}
					{/*if (this.state.position.y === rowIndex && this.state.position.x == cellIndex) {*/}
					{/*return <td key={cellIndex}*/}
					{/*className={classnames('selected', this.getCellClassName(cell.type))}>*/}
					{/*<input type='text' ref={(node)=>this.selected = node} defaultValue={cell.data}*/}
					{/*onChange={this.onInput}/>*/}
					{/*</td>*/}
					{/*} else {*/}
					{/*return <td key={cellIndex} onClick={this.onClick.bind(this,rowIndex,cellIndex)}*/}
					{/*className={classnames(this.getCellClassName(cell.type))}>{cell.data}</td>*/}
					{/*}*/}

					{/*})}*/}


					{/*</tr>*/}
					{/*})}*/}

					{/*<tr>*/}
					{/*<td className='table_title' colSpan='18'>Штрафы и премии</td>*/}
					{/*</tr>*/}
					{/*<tr>*/}
					{/*{this.state.rows[0].map((cell, index)=> {*/}
					{/*return <td key={index} className={this.getCellClassName(cell.type)}>{cell.name}</td>*/}
					{/*})}*/}
					{/*</tr>*/}


					</tbody>

				</table>
			</div>
		)
	}
}
