import React, { Component, PropTypes } from 'react';
import DefaultCell from './cells/DefaultCell';
export default class Cell extends Component {

	render() {
		const {type, children, data, style} = this.props;
		switch (type) {
			default:
				return (
					<th className={style.color} width={style.width}>
						<DefaultCell data={data}>{children}</DefaultCell>
					</th>
				)
		}
	}
}
