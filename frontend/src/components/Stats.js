import React, { Component, PropTypes } from 'react';
import styles from './Stats.css';

export default class Stats extends Component {

	render() {
		const {money, guests, newbie, time, rewards} = this.props;
		return (
			<tr className={styles.Stats}>
				<td></td><td></td>
				<td>{money}</td>
				<td></td>
				<td className="guests">{guests} гостей</td>
				<td></td>
				<td className="newbie">{newbie} / {guests-newbie}</td>
				<td></td><td></td><td></td><td></td><td></td>
				<td className="time">{time} ч.</td>
				<td></td>
				{rewards.map((reward, index)=>{
					return <td className="reward" key={index}>{reward}</td>
				})}

			</tr>
		)
	}
}
