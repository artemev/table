# webpack-seed

Start
```
npm install
npm start
```

Run tests
```
npm test
```

Lint:
```
npm run lint
```
