'use strict';

let ExtractTextPlugin = require("extract-text-webpack-plugin");
const webpack = require('webpack');
const autoprefixer = require('autoprefixer');
const precss = require('precss');

const NODE_ENV = process.env.NODE_ENV || 'development';

const isDev = () => NODE_ENV === 'development';
const hash = (pref, suf) => isDev() ? `${pref}.${suf}` : `${pref}.${suf}`;

module.exports = {
	context: __dirname + '/frontend/src',
	entry: './index',
	output: {
		path: __dirname + '/public',
		publicPath: '/',
		filename: hash('[name]', 'js'),
		chunkFilename: hash('[id]', 'js'),
		library: '[name]',
	},


	plugins: [
		new webpack.NoErrorsPlugin(),
		new webpack.HotModuleReplacementPlugin(),
		new webpack.DefinePlugin({
			NODE_ENV: JSON.stringify(NODE_ENV),
			LANG: JSON.stringify('en'),
		}),
		// new webpack.optimize.CommonsChunkPlugin({
		// 	name:'common'
		// })
		new ExtractTextPlugin('[name].css', {allChunks: true})
	],


	module: {
		preLoaders: [
			{
				test: /\.js$/,
				exclude: /\.spec\.js$/,
				loaders: ['eslint'],
				include: [
					__dirname + "/frontend"
				]
			}
		],

		loaders: [
			{
				test: /\.js$/,
				loader: 'babel-loader',
				exclude: /\/node_modules\//,
				query: {
					presets: ['es2015', 'stage-0', 'react'],
					plugins: ["transform-decorators-legacy"],
					cacheDirectory: true
				}
			},
			{
				test: /\.css$/,
				loader: ExtractTextPlugin.extract('!css-loader!postcss-loader')
			}
		]
	},

	postcss: function () {
		return [autoprefixer, precss];
	},

	watch: isDev(),

	watchOptions: {
		aggregateTimeout: 100
	},

	devtool: isDev() ? 'cheap-module-eval-source-map' : null,

	devServer: {
		contentBase: __dirname + '/backend',
		hot: true
	},
};

if (!isDev()) {
	module.exports.plugins.push(
		new webpack.optimize.UglifyJsPlugin({
				compress: {
					warnings: false,
					drop_console: true,
					unsafe: true,
				}
			}
		)
	)
}
